﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Battle_Bands.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Home()
        {
            return View();
        }

        public IActionResult Bands()
        {
            return View();
        }

        public IActionResult Festivals()
        {
            return View();
        }

        public IActionResult NewBand()
        {
            return View();
        }
        public IActionResult SignIn()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
